# ProductDetailQtyWidget

Before you add this module, you need to add the template "vendor/magento/module-catalog/view/frontend/templates/product/view/addtocart.phtml"
into your theme. ONce the theme's template is used, 
you may change the html

<input type="number"
   name="qty"
   id="qty"
   value="<?= /* @escapeNotVerified */ $block->getProductDefaultQty() * 1 ?>"
   title="<?= /* @escapeNotVerified */ __('Qty') ?>"
   class="input-text qty"
   data-validate="<?= $block->escapeHtml(json_encode($block->getQuantityValidators())) ?>"
   />
                       
to
                       
<button type="button" id="minusQty" value="1">-</button>
<input type="number"
       name="qty"
       id="qty"
       value="<?= /* @escapeNotVerified */ $block->getProductDefaultQty() * 1 ?>"
       title="<?= /* @escapeNotVerified */ __('Qty') ?>"
       class="input-text qty"
       data-validate="<?= $block->escapeHtml(json_encode($block->getQuantityValidators())) ?>"
       />
<button type="button" id="addQty" value="1">+</button>                       
                       
once you see both buttons in the frontend, you may start following the steps below                       
                       
1. clone the repository

3. create folder app/code/Mbs/ProductDetailQtyWidget when located at the root of the Magento site

4. copy the content of this repository within the folder

5. install the module php bin/magento setup:upgrade

6. load the product detail and verify 2 buttons are wrapping the qty input in the product detail view. Also, these buttons are now interacting with the qty input